# Wig 0.0.6 (in progress)
 * New options implemented
   * `+[no]nssearch`
   * `+[no]padding` ( experimental, sets the edns option but does not pad the question )
 * Other Changes
   * +idnin/+idnout defaults adapted to dig's behaviour to check if we are a TTY
   * updated miekg/dns to 1.1.62
# Wig 0.0.5
 * Other changes
   * updated miekg/dns to 1.1.58
   * reworked EDNS option output to look more like dig's
     * add human readable NSID
     * show human readable EXPIRE time
# Wig 0.0.4
 * New options implemented
   * `+[no]ttlunits`
   * `+[no]ttlid`
   * `+[no]class`
   * `+[no]crypto`
   * `+[no]split=###` ( default is not to split)
   * `+[no]expandaaaa`
   * `+[no]multiline`
   * `+[no]rrcomments`
   * `+[no]edns=###`
   * `+[no]ednsopt=###[:value]`
   * `+subnet=<addr>/<mask> , +nosubnet`
   * `+[no]zflag`
   * `+ednsflags=###`
   * `+[no]ednsnegotiation`
   * `+[no]expire`
   * `+[no]showbadcookie`
 * New flags implemented
   * `-m` ( use `go tool trace wig-trace.out` to analyze the trace)
 * Other changes
   * set AD bit in query by default (RFC6840, 5.7 )
# Wig 0.0.3
 * New options implemented
   * `+tries`
   * `+retry`
 * New flags implemented
   * `-b`
   * `-4`
   * `-6`
   * `-v` 
 * Other changes
   * if the server resolves to multiple addresses, wig now tries all of them
   * udp queries are now retried on error 
   * truncated udp responses are retried over TCP 
   * we now read `~/.digrc` for additional command line options
   * use better resolver detection from https://github.com/mr-karan/doggo/
# Wig 0.0.2
 * New options implemented:
   * `+[no]short`
   * `+[no]tcp`
   * `+qid=###`
   * `+onesoa`
   * `+[no]idnin`
   * `+[no]idnout`
   * `+[no]vc`
   * `+[no]header-only`
   * `+[no]opcode`
 * New flags implemented
   * `-u`
 * Other changes:
   * support IXFR (`-t ixfr=<serial>`)
   * use windows api instead of powershell to get the default resolver
   * show used protocol in stats (UDP, TCP, HTTPS, TLS)
   * with `+qr`, show the question before it is actually sent


# Wig 0.0.1

 * initial version
