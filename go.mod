module leetdreams.ch/wig/v2

go 1.22.0

toolchain go1.23.2

require (
	github.com/miekg/dns v1.1.63
	github.com/stretchr/testify v1.8.4
	golang.org/x/net v0.35.0
	golang.org/x/sys v0.30.0
	golang.org/x/term v0.29.0
)

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	golang.org/x/mod v0.23.0 // indirect
	golang.org/x/sync v0.11.0 // indirect
	golang.org/x/text v0.22.0 // indirect
	golang.org/x/tools v0.30.0 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)
