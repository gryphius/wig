package main

import (
	"time"

	"github.com/miekg/dns"
)

type ResponseContext struct {
	// the response message
	response *dns.Msg

	// the round trip time
	rtt time.Duration

	// the ip address of the server that responded
	serverip string

	// the port of the server that responded
	serverport uint16
}

func newResponseContext(response *dns.Msg, rtt time.Duration) *ResponseContext {
	return &ResponseContext{
		response: response,
		rtt:      rtt,
	}
}
