//go:build !windows
// +build !windows

package main

import (
	"bufio"
	"os"
	"strings"
)

func GetSystemResolvers() ([]string, error) {
	return GetSystemResolvers_Unix()
}

func GetSystemResolvers_Unix() ([]string, error) {
	// read /etc/resolv.conf
	// if nameserver is set, return that

	fp, err := os.Open("/etc/resolv.conf")
	if err != nil {
		return nil, err
	}
	defer fp.Close()

	var nameservers []string

	scanner := bufio.NewScanner(fp)
	for scanner.Scan() {
		line := scanner.Text()
		if strings.HasPrefix(line, "nameserver") {
			fields := strings.Fields(line)
			if len(fields) == 2 {
				nameservers = append(nameservers, fields[1])
			}
		}
	}
	return nameservers, nil
}
