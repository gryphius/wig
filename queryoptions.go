package main

import "github.com/miekg/dns"

// import miekg dns

type QueryOptions struct {
	// server hostname, if specified
	server_hostname string

	// list of ip addresses to query
	server_ips []string

	// port
	serverport uint16

	// local bind address, default is ""
	localbind string

	// local bind port
	localbindport uint16

	// query name
	name string

	// query type
	qtype uint16

	// query class
	qclass uint16

	// tcp or udp
	tcp bool

	// dnssec
	do bool

	// recursion desired
	rd bool

	// checking disabled
	cd bool

	// authoritative answer
	aa bool

	// authenticated data
	ad bool

	// recursion available
	ra bool

	// truncated
	tc bool

	// edns buffer size
	ednsBufSize      uint16
	nsid             bool
	ednsCookie       string
	ednsVersion      uint8
	ednsClientSubnet string
	ednsFlags        uint16 // z-bits
	ednsExpire       bool
	ednsNegotiation  bool
	ednsPadding      uint8 // padding length

	ednsopt []dns.EDNS0

	// timeout
	timeout uint16

	// tsig
	tsig_keyname string
	tsig_secret  string
	tsig_algo    string

	// show question
	qr bool

	// question id
	qid uint16

	// tls options
	tls_enable          bool
	tls_validate        bool
	tls_ca              string
	tls_hostname        string
	tls_client_certfile string
	tls_client_keyfile  string

	// doh options
	doh_enable    bool
	doh_use_https bool
	doh_path      string
	doh_use_get   bool

	// display options
	display_answer     bool
	display_authority  bool
	display_additional bool
	display_class      bool
	display_cmd        bool
	display_comments   bool
	display_crypto     bool
	display_question   bool
	display_rrcomments bool
	display_short      bool
	display_stats      bool
	display_ttlid      bool
	display_ttlunits   bool
	display_multi_line bool
	display_badcookie  bool
	display_search     bool
	split              uint16

	// expand AAAA
	expand_aaaa bool

	// onesoa
	onesoa bool

	// idnout
	idnout bool

	// usec
	usec bool

	// header_only
	header_only bool

	// ixfr serial
	ixfr_serial uint32

	// opcode
	opcode int

	// force ipv4 or ipv6
	force_ipv4 bool
	force_ipv6 bool

	// number of tries per (UDP) query
	tries int

	// z-flag
	zflag bool

	// nssearch
	nssearch bool
}

func NewQueryOptions() QueryOptions {
	return QueryOptions{
		tries:               3,
		serverport:          53,
		localbind:           "",
		name:                GlobalQueryOpt_name,
		qtype:               GlobalQueryOpt_qtype,
		qclass:              GlobalQueryOpt_qclass,
		ednsBufSize:         DEFAULT_EDNS_BUFSIZE,
		timeout:             5,
		rd:                  true,
		tls_ca:              "",
		tls_hostname:        "",
		tls_client_certfile: "",
		tls_client_keyfile:  "",
		doh_use_https:       true,
		doh_path:            "",
		ednsClientSubnet:    "",
		opcode:              dns.OpcodeQuery,
		// from globals

	}
}
