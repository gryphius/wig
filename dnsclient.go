package main

import (
	"bytes"
	"context"
	"crypto/tls"
	"crypto/x509"
	"encoding/base64"
	"encoding/hex"
	"fmt"
	"io"
	"net"
	"net/http"
	"net/url"
	"os"
	"strings"
	"time"

	"github.com/miekg/dns"
)

func buildQuery(options QueryOptions) *dns.Msg {

	msg := new(dns.Msg)

	if !options.header_only {
		msg.SetQuestion(dns.Fqdn(options.name), options.qtype)
		// query class
		msg.Question[0].Qclass = options.qclass
	}

	// EDNS0 stuff
	need_edns := false
	if options.ednsBufSize != 0 || options.do || options.nsid || options.ednsCookie != "" || options.ednsClientSubnet != "" || len(options.ednsopt) > 0 || options.ednsPadding > 0 {
		need_edns = true
	}

	if need_edns {
		opt := new(dns.OPT)
		opt.Hdr.Name = "."
		opt.Hdr.Rrtype = dns.TypeOPT
		opt.SetVersion(options.ednsVersion)
		opt.SetUDPSize(options.ednsBufSize)
		if options.do {
			opt.SetDo()
		}

		if options.nsid {
			// Create the NSID option (type 3) with data (e.g., NSID value or empty byte slice)
			nsidOption := new(dns.EDNS0_NSID)
			nsidOption.Code = dns.EDNS0NSID
			nsidOption.Nsid = ""
			opt.Option = append(opt.Option, nsidOption)
		}

		if options.ednsCookie != "" {
			cookie := new(dns.EDNS0_COOKIE)
			cookie.Code = dns.EDNS0COOKIE
			cookie.Cookie = options.ednsCookie
			opt.Option = append(opt.Option, cookie)
		}

		if options.ednsClientSubnet != "" {
			err := addEDNSClientSubnet(opt, options.ednsClientSubnet)
			if err != nil {
				panic(err)
			}
		}

		if options.ednsExpire {
			addEDNSExpire(opt)
		}

		if options.ednsPadding > 0 {
			addEDNSPadding(opt, options.ednsPadding)
		}

		if options.ednsFlags != 0 { // lower 24 bits are flags
			opt.SetZ(options.ednsFlags)
		}

		// add edns options
		opt.Option = append(opt.Option, options.ednsopt...)

		msg.Extra = append(msg.Extra, opt)
	}

	// question ID
	if options.qid != 0 {
		msg.Id = options.qid
	}

	// flags
	msg.RecursionDesired = options.rd
	msg.CheckingDisabled = options.cd
	msg.AuthenticatedData = options.ad
	msg.Authoritative = options.aa
	msg.RecursionAvailable = options.ra
	msg.Truncated = options.tc

	// z-flag
	if options.zflag {
		msg.MsgHdr.Zero = true
	}

	// opcode
	msg.Opcode = options.opcode

	return msg
}

func sendQuery(question *dns.Msg, options QueryOptions) (responseContext *ResponseContext, err error) {

	if options.doh_enable {

		responseContext, err = doDNSOverHTTPS(options, question)

	} else {
		responseContext, err = doDNSOverPlainOrTLS(options, question)
	}
	return responseContext, err
}

func makeDialer(options QueryOptions) net.Dialer {
	forceipversion := ""
	if options.force_ipv4 {
		forceipversion = "4"
	} else if options.force_ipv6 {
		forceipversion = "6"
	}

	dialer := net.Dialer{}
	if options.localbind != "" {
		localAddr := options.localbind
		// if localbind is an ip6 address, wrap in []
		localAddr = addV6Brackets(localAddr)
		// by  default the port is 0, which tells the OS to use a random one
		localAddr = localAddr + ":" + fmt.Sprint(options.localbindport)

		if options.tcp || options.tls_enable || options.doh_enable {
			netProto := "tcp" + forceipversion
			localTCPAddr, err := net.ResolveTCPAddr(netProto, localAddr)
			if err != nil {
				panic(err)
			}
			dialer.LocalAddr = localTCPAddr
		} else {
			netProto := "udp" + forceipversion
			localUDPAddr, err := net.ResolveUDPAddr(netProto, localAddr)
			if err != nil {
				panic(err)
			}
			dialer.LocalAddr = localUDPAddr
		}

	}
	return dialer
}

func makeDNSClient(options QueryOptions) *dns.Client {

	forceipversion := ""
	if options.force_ipv4 {
		forceipversion = "4"
	} else if options.force_ipv6 {
		forceipversion = "6"
	}

	client := new(dns.Client)
	if options.tls_enable {
		// set up TLS Options
		tlsConfig := tls.Config{}
		if options.tls_hostname != "" {
			tlsConfig.ServerName = options.tls_hostname
		} else {
			tlsConfig.ServerName = addV6Brackets(options.server_hostname)
		}
		if options.tls_validate {
			tlsConfig.InsecureSkipVerify = false
			if options.tls_ca != "" {
				caCert, err := os.ReadFile(options.tls_ca)
				if err != nil {
					panic(err)
				}
				caCertPool := x509.NewCertPool()
				caCertPool.AppendCertsFromPEM(caCert)
				tlsConfig.RootCAs = caCertPool
			}
		} else {
			tlsConfig.InsecureSkipVerify = true
		}
		if options.tls_client_certfile != "" && options.tls_client_keyfile != "" {
			cert, err := tls.LoadX509KeyPair(options.tls_client_certfile, options.tls_client_keyfile)
			if err != nil {
				panic(err)
			}
			tlsConfig.Certificates = []tls.Certificate{cert}
		}
		if options.ednsPadding > 0 {
			tlsConfig.DynamicRecordSizingDisabled = true
			// can we set padding block size?
		}

		client.Net = "tcp-tls"
		client.TLSConfig = &tlsConfig
	} else if options.tcp {
		client.Net = "tcp" + forceipversion
	} else {
		client.Net = "udp" + forceipversion
	}
	client.Timeout = time.Duration(options.timeout) * time.Second
	return client
}

func doDNSOverPlainOrTLS(options QueryOptions, msg *dns.Msg) (*ResponseContext, error) {
	var responseContext *ResponseContext
	var err error

	// set up the dialer
	dialer := makeDialer(options)

	// set up the client
	client := makeDNSClient(options)

	client.Dialer = &dialer

	var UDP_MODE = true
	if options.tcp || options.tls_enable {
		UDP_MODE = false
	}

	// loop through all the server ips
	for _, serverip := range options.server_ips {
		serverPort := addV6Brackets(serverip) + ":" + fmt.Sprint(options.serverport)

		// if UDP mode, use retries

		for i := 0; i < options.tries; i++ {
			reponse, rtt, err := client.Exchange(msg, serverPort)
			responseContext = newResponseContext(reponse, rtt)
			responseContext.serverip = serverip
			responseContext.serverport = options.serverport

			// if the response is truncated, retry with TCP
			if responseContext.response != nil && responseContext.response.Truncated {
				UDP_MODE = false
				// LOG TRUNCATED
				fmt.Println(";; Truncated, retrying with TCP")
				options.tcp = true
				client = makeDNSClient(options)
				// reduce i by one, so we retry with TCP
				i = i - 1
				continue
			}

			// if response is BADVERS and has a OPT record and lower edns version, retry with lower edns version
			if responseContext.response != nil && responseContext.response.Rcode == dns.RcodeBadVers && options.ednsNegotiation && options.ednsVersion > 0 {
				ansopt := responseContext.response.IsEdns0()
				if ansopt != nil && ansopt.Version() < options.ednsVersion {
					options.ednsVersion = ansopt.Version()
					// LOG BADVERS
					fmt.Println(";; BADVERS, retrying with EDNS version", options.ednsVersion)
					continue
				}
			}

			// check if we get a BADCOOKIE response, retry with new cookie
			if responseContext.response != nil && responseContext.response.Rcode == dns.RcodeBadCookie {
				// LOG BADCOOKIE
				fmt.Println(";; BADCOOKIE, retrying with new cookie ")

				// if display_badcookie is set, print the message using digOutput
				if options.display_badcookie {
					digOutput(options, responseContext)
				}

				for _, rr := range responseContext.response.Extra {
					if opt, ok := rr.(*dns.OPT); ok {
						for _, option := range opt.Option {
							if cookie, ok := option.(*dns.EDNS0_COOKIE); ok {
								options.ednsCookie = cookie.Cookie
								break
							}
						}
					}
				}
				continue
			}

			if err == nil {
				return responseContext, nil
			}
			// print server ip and error
			fmt.Println(";; SERVER: " + serverip + " (" + err.Error() + ")")

			// if we are not in UDP mode, break out of the loop, we dont need retries
			if !UDP_MODE {
				break
			}
		}

	}
	return responseContext, err
}

func doDNSOverHTTPS(options QueryOptions, msg *dns.Msg) (responseContext *ResponseContext, err error) {

	// Convert dns.Question to wire format
	wire, err := msg.Pack()
	if err != nil {
		return nil, err
	}

	var doh_full_url string
	var doh_path = options.doh_path
	if doh_path == "" {
		doh_path = "/dns-query"
	}
	serverport_string := fmt.Sprint(options.serverport)

	for _, serverip := range options.server_ips {
		// build doh full url from server

		var doh_host = addV6Brackets(options.server_hostname)

		if options.doh_use_https {
			doh_full_url = "https://" + doh_host + ":" + serverport_string + doh_path
		} else {
			doh_full_url = "http://" + doh_host + ":" + serverport_string + doh_path
		}

		dialer := makeDialer(options)

		// Create a custom transport
		transport := &http.Transport{
			DialContext: func(ctx context.Context, network, addr string) (net.Conn, error) {
				return dialer.DialContext(ctx, network, addV6Brackets(serverip)+":"+serverport_string)
			},
		}

		httpclient := &http.Client{
			Transport: transport,
			Timeout:   time.Duration(options.timeout) * time.Second,
		}

		// Prepare HTTP request
		req, err := http.NewRequest("GET", doh_full_url, nil)
		if err != nil {
			return nil, err
		}
		req.Header.Set("accept", "application/dns-message")
		req.Host = options.server_hostname

		// Change method and body if POST is used
		if !options.doh_use_get {
			req.Method = "POST"
			req.Body = io.NopCloser(bytes.NewReader(wire))
			req.Header.Set("content-type", "application/dns-message")
		} else {
			urlObj, err := url.Parse(doh_full_url)
			if err != nil {
				return nil, err
			}
			query := urlObj.Query()
			query.Set("dns", base64.RawURLEncoding.EncodeToString(wire))
			urlObj.RawQuery = query.Encode()
			req.URL = urlObj
		}

		// print url
		//fmt.Println(";; URL: " + req.URL.String())

		// Send HTTP request
		start := time.Now()

		resp, err := httpclient.Do(req)
		if err != nil {
			return nil, err
		}
		defer resp.Body.Close()

		// Calculate request duration
		duration := time.Since(start)

		// check response status code
		if resp.StatusCode != 200 {
			responseContext = new(ResponseContext)
			responseContext.rtt = duration
			err = fmt.Errorf("HTTP status code %d", resp.StatusCode)
			// print server ip and error
			fmt.Println(";; SERVER: " + serverip + " (" + err.Error() + ")")
			continue
		}

		// Read response body
		wire, err = io.ReadAll(resp.Body)
		if err != nil {
			responseContext = new(ResponseContext)
			responseContext.rtt = duration
			// print server ip and error
			fmt.Println(";; SERVER: " + serverip + " (" + err.Error() + ")")
			continue
		}

		// Unpack wire format to dns.Message
		dnsAnswer := new(dns.Msg)
		err = dnsAnswer.Unpack(wire)
		if err != nil {
			responseContext = new(ResponseContext)
			responseContext.rtt = duration
			// print server ip and error
			fmt.Println(";; SERVER: " + serverip + " (" + err.Error() + ")")
			continue
		}

		// Create ResponseContext
		responseContext = new(ResponseContext)
		responseContext.response = dnsAnswer
		responseContext.rtt = duration
		responseContext.serverip = serverip
		responseContext.serverport = options.serverport
		return responseContext, nil

	}
	return responseContext, err
}

func doXFR(options QueryOptions) (chan *dns.Envelope, error) {
	msg := new(dns.Msg)
	serverPort := addV6Brackets(options.server_hostname) + ":" + fmt.Sprint(options.serverport)
	if options.ixfr_serial != 0 {
		msg.SetIxfr(dns.Fqdn(options.name), options.ixfr_serial, ".", ".")
	} else {
		msg.SetAxfr(dns.Fqdn(options.name))
	}

	tr := new(dns.Transfer)

	if options.tsig_keyname != "" {
		tsig_fqdn := dns.Fqdn(options.tsig_keyname)
		tr.TsigSecret = map[string]string{tsig_fqdn: options.tsig_secret}
		// note: tsig algo must be fqdn as well!
		msg.SetTsig(tsig_fqdn, dns.Fqdn(options.tsig_algo), 300, time.Now().Unix())
	}

	return tr.In(msg, serverPort)
}

func addEDNSClientSubnet(opt *dns.OPT, ipOrCIDR string) error {
	// if ipOrCIDR is 0, set an empty IP and zero prefix
	if ipOrCIDR == "0" {
		ipOrCIDR = "0.0.0.0/0"
	}

	// Parse the IP and CIDR mask
	ip, ipNet, err := net.ParseCIDR(ipOrCIDR)
	if err != nil {
		// If CIDR parsing fails, try parsing as a simple IP
		ip = net.ParseIP(ipOrCIDR)
		if ip == nil {
			return fmt.Errorf("invalid IP or CIDR format")
		}
	}

	// Create an EDNS0_SUBNET option
	ecs := new(dns.EDNS0_SUBNET)
	ecs.Code = dns.EDNS0SUBNET
	ecs.SourceScope = 0

	if strings.Contains(ipOrCIDR, ":") {
		ecs.Family = 2 // IPv6
		ecs.SourceNetmask = 128
	} else {
		ecs.Family = 1 // IPv4
		ecs.SourceNetmask = 32
	}

	if ipNet != nil {
		ones, _ := ipNet.Mask.Size()
		ecs.SourceNetmask = uint8(ones)
	}

	ecs.Address = ip

	// Add the EDNS0_SUBNET option to the OPT record
	opt.Option = append(opt.Option, ecs)

	return nil
}

func addEDNSExpire(opt *dns.OPT) {
	// Create an EDNS0_EXPIRE option
	exp := new(dns.EDNS0_EXPIRE)
	exp.Code = dns.EDNS0EXPIRE
	exp.Empty = true

	// Add the EDNS0_EXPIRE option to the OPT record
	opt.Option = append(opt.Option, exp)
}

func addEDNSPadding(opt *dns.OPT, padding uint8) {
	// Create an EDNS0_PADDING option
	pad := new(dns.EDNS0_PADDING)
	pad.Padding = make([]byte, padding)

	// Add the EDNS0_PADDING option to the OPT record
	opt.Option = append(opt.Option, pad)
}

// make a dns.OPT from its codepoint and a hex string
func makeEDNSOption(codepoint uint16, hexstring string) (dns.EDNS0, error) {
	// convert hex string to bytes
	bytes, err := hexStringToBytes(hexstring)
	if err != nil {
		return nil, err
	}

	// create the option
	opt := new(dns.EDNS0_LOCAL)
	opt.Code = codepoint
	opt.Data = bytes

	return opt, nil
}

// convert a hex string to bytes
func hexStringToBytes(hexstring string) ([]byte, error) {
	// convert hex string to bytes
	bytes, err := hex.DecodeString(hexstring)
	if err != nil {
		return nil, err
	}

	return bytes, nil
}

func nssearch(options QueryOptions) {
	completed_serverips := []string{}
	nssearch_delegation(options, GetRootIPs(), &completed_serverips, 0)
}

func nssearch_delegation(options QueryOptions, server_ips []string, completed_serverips *[]string, querycount int) {
	MAX_QUERYCOUNT := 100
	if querycount > MAX_QUERYCOUNT {
		fmt.Println(";; Max query count reached, stopping recursion")
		return
	}
	for _, serverip := range server_ips {
		querycount++
		// if we have already queried this server, skip it
		if stringInSlice(serverip, *completed_serverips) {
			continue
		}
		*completed_serverips = append(*completed_serverips, serverip)
		soa, delegations, err := nssearch_delegation_single(options, serverip)

		if err != nil {
			fmt.Println(";; SERVER: " + serverip + " (" + err.Error() + ")")
			continue
		}
		if soa != nil { // print server ip and soa
			template := "SOA %s from server %s"
			soa_content := fmt.Sprintf("%s %s %v %v %v %v %v", soa.Ns, soa.Mbox, soa.Serial, soa.Refresh, soa.Retry, soa.Expire, soa.Minttl)
			fmt.Println(fmt.Sprintf(template, soa_content, serverip))
			continue
		}
		if delegations != nil {
			// get all ips from the delegations
			delegation_ips := []string{}
			for _, ns := range *delegations {
				new_ips, err := lookupIPs(ns.Ns)
				if err != nil {
					fmt.Println(";; SERVER: " + serverip + " (" + err.Error() + ")")
					continue
				}
				delegation_ips = append(delegation_ips, new_ips...)
			}
			// recurse with the new ips
			nssearch_delegation(options, delegation_ips, completed_serverips, querycount)
		}

	}
}

func stringInSlice(a string, list []string) bool {
	for _, b := range list {
		if b == a {
			return true
		}
	}
	return false
}

func nssearch_delegation_single(options QueryOptions, ip string) (soa *dns.SOA, delegation *[]dns.NS, err error) {

	// filter ipv6 if needed
	if options.force_ipv4 && net.ParseIP(ip).To4() == nil {
		return nil, nil, nil
	}

	// filter ipv4 if needed
	if options.force_ipv6 && net.ParseIP(ip).To4() != nil {
		return nil, nil, nil
	}

	options.qtype = dns.TypeSOA
	options.rd = false
	options.server_ips = []string{ip}
	query := buildQuery(options)

	responseContext, err := sendQuery(query, options)
	if err != nil {
		return nil, nil, err
	}

	if responseContext.response == nil {
		return nil, nil, fmt.Errorf("no response")
	}

	if responseContext.response.Rcode != dns.RcodeSuccess {
		return nil, nil, fmt.Errorf(";; SERVER: " + responseContext.serverip + " (" + dns.RcodeToString[responseContext.response.Rcode] + ")")
	}

	for _, rr := range responseContext.response.Answer {
		if soa, ok := rr.(*dns.SOA); ok {
			// if soa name is the same as the query name, return it
			if soa.Hdr.Name == query.Question[0].Name {
				return soa, nil, nil
			}
		}
	}

	// initialize return slice for delegation
	delegation = &[]dns.NS{}
	for _, rr := range responseContext.response.Ns {
		if ns, ok := rr.(*dns.NS); ok {
			*delegation = append(*delegation, *ns)
		}
	}
	return nil, delegation, nil

}
