package main

import (
	"net"
	"strings"
)

// given a hostname, returns a list of ip addresses
func lookupIPs(hostname string) ([]string, error) {
	ips := []string{}

	// if hostname is an ip address, return it
	if net.ParseIP(hostname) != nil {
		ips = append(ips, hostname)
		return filterIPs(ips), nil
	}

	// otherwise, resolve the hostname
	addrs, err := net.LookupHost(hostname)
	if err != nil {
		return ips, err
	}
	ips = append(ips, addrs...)

	return filterIPs(ips), nil
}

// filters a slice of ip addresses, returning only those that are valid for the given protocol
func filterIPs(ips []string) []string {
	validips := []string{}

	for _, ip := range ips {
		// if -4 is set, do not include ipv6 addresses
		if GlobalQueryOpt_ipv4 && net.ParseIP(ip).To4() == nil {
			continue
		}

		// if -6 is set, do not include ipv4 addresses
		if GlobalQueryOpt_ipv6 && net.ParseIP(ip).To4() != nil {
			continue
		}

		validips = append(validips, ip)

	}

	return validips
}

func addV6Brackets(ip string) string {
	// check if ip is definitely a IPv6 address and not a hostname or ipv4
	if strings.Contains(ip, ":") && !strings.Contains(ip, ".") {
		return "[" + ip + "]"
	}
	return ip
}
