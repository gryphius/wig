List of dig options and current compatibility/implementation status


| Option | status | comment |
| ------ | ------ | ------- |
| `@server` | :white_check_mark: | |
| `q-claas`  | :white_check_mark: | |
| `q-type`  | :white_check_mark: | |
| `axfr` | :white_check_mark: | |
| `ixfr` | :white_check_mark: | added in 0.0.2 |
| `-4` | :white_check_mark: | added in 0.0.3 |
| `-6` | :white_check_mark: | added in 0.0.3 |
| `-b address[#port]` | :white_check_mark: | added in 0.0.3 |
| `-c class` | :white_check_mark: | |
| `-f filename` | :x: | |
| `-k keyfile` | :white_check_mark: | |
| `-m` | :white_check_mark: |  added in 0.0.4, using go's trace package |
| `-p port` | :white_check_mark: | |
| `-q name` |  :white_check_mark: | |
| `-r` | :white_check_mark: | added in 0.0.3 |
| `-t type` |  :white_check_mark: | |
| `-u`  | :white_check_mark: | added in 0.0.2 |
| `-x dot-notation` | :white_check_mark: | |
| `-y [hmac:]name:key`  | :white_check_mark: | |
| `+[no]aaflag`  |  :white_check_mark: | |
| `+[no]aaonly`  | :white_check_mark: | |
| `+[no]additional`  | :white_check_mark: | |
| `+[no]adflag`  | :white_check_mark: | |
| `+[no]all`  | :white_check_mark: | |
| `+[no]answer`  | :white_check_mark: | |
| `+[no]authority`  | :white_check_mark: | |
| `+[no]badcookie`  | :x: | |
| `+[no]besteffort`  | :x: | |
| `+bufsize[=###]`  | :white_check_mark: | |
| `+[no]cdflag`  | :white_check_mark: | |
| `+[no]class `  | :white_check_mark: |  added in 0.0.4 |
| `+[no]cmd`  | :white_check_mark: | |
| `+[no]comments`  | :white_check_mark: | |
| `+[no]cookie`  | :white_check_mark: | |
| `+[no]crypto`  | :white_check_mark: |  added in 0.0.4 |
| `+[no]defname`  | :x: | |
| `+[no]dns64prefix`  | :x: | |
| `+[no]dnssec`  | :white_check_mark: | |
| `+domain=###`  | :x: | |
| `+[no]edns[=###]`  | :white_check_mark: | added in 0.0.4 |
| `+ednsflags=###`  |  :white_check_mark: | added in 0.0.4 |
| `+[no]ednsnegotiation`  | :white_check_mark: | added in 0.0.4 |
| `+[no]ednsopt=###[:value]`  | :white_check_mark: | added in 0.0.4 |
| `+[no]expandaaaa`  | :white_check_mark: | added in 0.0.4 |
| `+[no]expire`  | :white_check_mark: | added in 0.0.4 |
| `+[no]fail`  | :x: | |
| `+[no]header-only`  | :white_check_mark: | added in 0.0.2 |
| `+[no]https[=###]`  | :white_check_mark: | |
| `+[no]https-get`  | :white_check_mark: | |
| `+[no]http-plain[=###]`  | :white_check_mark: | |
| `+[no]http-plain-get`  | :white_check_mark: | |
| `+[no]identify`  | :x: | |
| `+[no]idnin`  | :white_check_mark: | added in 0.0.2 |
| `+[no]idnout`  | :white_check_mark: | added in 0.0.2 |
| `+[no]ignore`  | :x: | |
| `+[no]keepalive`  | :x: | |
| `+[no]keepopen`  | :x: | |
| `+[no]multiline`  |:white_check_mark: | added in 0.0.4 |
| `+ndots=###`  | :x: | |
| `+[no]nsid`  | :white_check_mark: | |
| `+[no]nssearch`  | :white_check_mark: | added in 0.0.6 |
| `+[no]onesoa`  |  :white_check_mark: | added in 0.0.2 |
| `+[no]opcode=###`  |:white_check_mark: | added in 0.0.2 |
| `+padding=###`  | :white_check_mark: | added in 0.0.6 (experimental: sets edns option, but does not pad question with the desired block size ) |
| `+qid=###`  | :white_check_mark: |  added in 0.0.2 |
| `+[no]qr`  | :white_check_mark: | |
| `+[no]question`  | :white_check_mark: | |
| `+[no]raflag`  | :white_check_mark: | |
| `+[no]rdflag`  | :white_check_mark: | |
| `+[no]recurse`  | :white_check_mark: | |
| `+retry=###`  |  :white_check_mark: | added in 0.0.3 |
| `+[no]rrcomments`  | :white_check_mark: | added in 0.0.4 |
| `+[no]search`  | :x: | |
| `+[no]short`  | :white_check_mark: | added in 0.0.2 |
| `+[no]showbadcookie`  | :white_check_mark: | added in 0.0.4 |
| `+[no]showsearch`  | :x: | |
| `+[no]split=##`  | :white_check_mark: | added in 0.0.4 . Unlike dig, our default is 0  |
| `+[no]stats`  | :white_check_mark: | |
| `+subnet=addr`  | :white_check_mark: | added in 0.0.4 |
| `+[no]tcflag`  | :white_check_mark: | |
| `+[no]tcp`  | :white_check_mark: | added in 0.0.2 |
| `+timeout=###`  | :white_check_mark: | |
| `+[no]tls`  | :white_check_mark: | |
| `+[no]tls-ca[=file]`  | :white_check_mark: | |
| `+[no]tls-hostname=hostname`  | :white_check_mark: | |
| `+[no]tls-certfile=file`  | :white_check_mark: | |
| `+[no]tls-keyfile=file`  | :white_check_mark: | |
| `+[no]trace`  | :x: | |
| `+tries=###`  |  :white_check_mark: | added in 0.0.3 |
| `+[no]ttlid`  | :white_check_mark: |  added in 0.0.4 |
| `+[no]ttlunits`  | :white_check_mark: |  added in 0.0.4 |
| `+[no]unknownformat`  | :x: | |
| `+[no]vc`  | :white_check_mark: | added in 0.0.2 |
| `+[no]yaml`  | :x: | |
| `+[no]zflag`  | :white_check_mark: |  added in 0.0.4 |
| `global d-opts vs local d-opts` | :x: | currently only one optionset supported|
| `-h`  | :white_check_mark: | |
| `-v`  | :white_check_mark: | added in 0.0.3 |

