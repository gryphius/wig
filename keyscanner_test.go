package main

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestParseTSIG(t *testing.T) {
	keycontent := `key "tsig-zonedata-li-public-21-01" { algorithm hmac-sha512;
		secret "t8GgeCn+fhPaj+cRy1epox2Vj4hZ45ax6v3rQCkkfIQNg5fsxuU23QM5mzz+BxJ4kgF/jiQyBDBvL+XWPE6oCQ=="; 
	}; `

	algo, keyname, secret, err := ParseTSIG(keycontent)
	if err != nil {
		t.Fatal(err)
	}
	assert.Equal(t, "hmac-sha512.", algo)
	assert.Equal(t, "tsig-zonedata-li-public-21-01.", keyname)
	assert.Equal(t, "t8GgeCn+fhPaj+cRy1epox2Vj4hZ45ax6v3rQCkkfIQNg5fsxuU23QM5mzz+BxJ4kgF/jiQyBDBvL+XWPE6oCQ==", secret)

}
