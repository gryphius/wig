package main

import (
	"fmt"
	"os"
	"strconv"
	"strings"

	"github.com/miekg/dns"
)

func ReadTSIGFile(filename string) (string, string, string, error) {
	fileContent, err := os.ReadFile(filename)
	if err != nil {
		fmt.Println(err)
		return "", "", "", err
	}

	content := string(fileContent)
	return ParseTSIG(content)
}

func ParseTSIG(content string) (string, string, string, error) {

	var err error
	var secret, algorithm, keyname string

	start := strings.Index(content, "{")
	end := strings.LastIndex(content, "}")
	if start == -1 || end == -1 {
		return "", "", "", fmt.Errorf("could not parse TSIG key")
	}

	contentBetweenBraces := content[start+1 : end]

	parts := strings.Split(contentBetweenBraces, ";")
	for _, part := range parts {
		part = strings.TrimSpace(part)

		if strings.Contains(part, "algorithm") {
			subparts := strings.Fields(part)
			algorithm, err = strconv.Unquote(subparts[1])
			if err != nil {
				algorithm = subparts[1] // no quotes
			}
		} else if strings.Contains(part, "secret") {
			subparts := strings.Fields(part)
			secret, err = strconv.Unquote(subparts[1])
			if err != nil {
				secret = subparts[1] // no quotes
			}
		}
	}

	preBraceContent := content[:start]
	preBraceParts := strings.Fields(preBraceContent)
	keyname, err = strconv.Unquote(preBraceParts[1])
	if err != nil {
		keyname = preBraceParts[1] // no quotes
	}

	keyname = dns.Fqdn(keyname)
	algorithm = dns.Fqdn(algorithm)
	return algorithm, keyname, secret, nil
}
