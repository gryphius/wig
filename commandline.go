package main

import (
	"bufio"
	"crypto/rand"
	"encoding/hex"
	"flag"
	"fmt"
	"net"
	"os"
	"os/user"
	"path/filepath"
	"strconv"
	"strings"

	"github.com/miekg/dns"
	"golang.org/x/term"
)

// d options can be global or local for each query
type DOptions struct {
	// cd flag
	cd bool

	// do flag
	do bool

	// rd flag
	rd bool

	// aa flag
	aa bool

	// ad flag
	ad bool

	// ra flag
	ra bool

	// tc flag
	tc bool

	// ednsbufsize
	ednsbufsize      uint16
	nsid             bool
	ednsCookie       string
	ednsVersion      uint8
	ednsClientSubnet string
	ednsFlags        uint16
	ednsExpire       bool
	ednsNegotiation  bool
	ednsPadding      uint8
	ednsopt          []dns.EDNS0

	// timeout
	timeout uint16

	// show question
	qr bool

	// tcp
	tcp bool

	// question id
	qid uint16

	// onesoa
	onesoa bool

	// header_only
	header_only bool

	// tls options
	tls_enable          bool
	tls_validate        bool
	tls_ca              string
	tls_hostname        string
	tls_client_certfile string
	tls_client_keyfile  string

	// doh options
	doh_enable    bool
	doh_use_https bool
	doh_path      string
	doh_use_get   bool

	// idn
	idnin  bool
	idnout bool

	split uint16

	// display options
	display_answer     bool
	display_authority  bool
	display_additional bool
	display_class      bool
	display_cmd        bool
	display_comments   bool
	display_crypto     bool
	display_question   bool
	display_rrcomments bool
	display_short      bool
	display_stats      bool
	display_ttlid      bool
	display_ttlunits   bool
	display_multi_line bool
	display_badcookie  bool
	display_search     bool

	expand_aaaa bool

	// opcode
	opcode int

	// tries, retries
	tries int

	// z-flag
	zflag bool

	// nssearch
	nssearch bool
}

func newDOptions() DOptions {
	cookie, err := GenerateEDNSCookie()
	if err != nil {
		panic(err)
	}

	isaTTY := term.IsTerminal(int(os.Stdout.Fd()))

	return DOptions{
		cd:                 false,
		do:                 false,
		rd:                 true,
		ad:                 true, // RFC 6840 5.7 says that AD bit should be set in the query to indicate the client understands the AD bit
		ednsbufsize:        DEFAULT_EDNS_BUFSIZE,
		timeout:            DEFAULT_TIMEOUT,
		ednsCookie:         cookie,
		display_answer:     true,
		display_authority:  true,
		display_additional: true,
		display_class:      true,
		display_cmd:        true,
		display_comments:   true,
		display_crypto:     true,
		display_question:   true,
		display_rrcomments: false,
		display_short:      false,
		display_stats:      true,
		display_ttlid:      true,
		display_ttlunits:   false,
		display_badcookie:  false,
		display_search:     true,
		idnin:              isaTTY,
		idnout:             isaTTY,
		tries:              3,
		ednsNegotiation:    true,
	}
}

type QueryServerOptCombo struct {
	DOptions DOptions
	Server   string
}

func NewQueryServerOptCombo() QueryServerOptCombo {
	return QueryServerOptCombo{
		DOptions: newDOptions(),
		Server:   "",
	}
}

// query options
var (
	// -v option shows version and exits
	GlobalQueryOpt_version bool = false

	// -4 option forces IPv4
	GlobalQueryOpt_ipv4 bool = false

	// -6 option forces IPv6
	GlobalQueryOpt_ipv6 bool = false

	// -p option sets the server port
	GlobalQueryOpt_ServerPort int = -1

	// GlobalQueryOpt_qtype is the query type
	GlobalQueryOpt_qtype     uint16 = dns.TypeA
	GLobalQueryOpt_qtype_str string = ""

	GlobalQueryOpt_qclass     uint16 = dns.ClassINET
	GlobalQueryOpt_qclass_str string = ""

	GlobalQueryOpt_name          string = "."
	GlobalQueryOpt_name_override string = ""

	GlobalQueryOpt_localbind string = ""

	GlobalQueryOpt_reverseAddr string = ""

	// TSIG
	GlobalQueryOpt_tsig = ""

	GlobalQueryOpt_tsigfile = ""

	// Store if name and type have been passed as cmdline argument. If not, ask root for NS by default.
	GlobalQueryOpt_HAVENAME bool = false
	GlobalQueryOpt_HAVETYPE bool = false

	GlobalQueryOpt_HAVEPORT bool = false

	GlobalQueryOpt_usec bool = false

	GlobalQueryOpt_ixfr_serial uint32 = 0

	GlobalQueryOpt_disabledigrc bool = false

	GlobalQueryOpt_memtrace bool = false
)

// parse command line args using flag package

func parseArgs() []QueryServerOptCombo {
	// -4 option forces IPv4
	flag.BoolVar(&GlobalQueryOpt_ipv4, "4", false, "(use IPv4 query transport only)")

	// -6 option forces IPv6
	flag.BoolVar(&GlobalQueryOpt_ipv6, "6", false, "(use IPv6 query transport only)")

	// -b option sets the local bind address
	flag.StringVar(&GlobalQueryOpt_localbind, "b", "", "local address to bind to")

	// -c option sets the query class
	flag.StringVar(&GlobalQueryOpt_qclass_str, "c", "", "(specify query class)")

	// -t option sets the query type
	flag.StringVar(&GLobalQueryOpt_qtype_str, "t", "", "(specify query type)")

	// -q option sets the query name
	flag.StringVar(&GlobalQueryOpt_name, "q", ".", "(specify query name)")

	// -y option sets the TSIG keyname, secret, and algo
	flag.StringVar(&GlobalQueryOpt_tsig, "y", "", "TSIG [hmac:]algo:secret")

	// -k option sets the TSIG keyname, secret, and algo from a file
	flag.StringVar(&GlobalQueryOpt_tsigfile, "k", "", "TSIG key file")

	// -x option sets the reverse address
	flag.StringVar(&GlobalQueryOpt_reverseAddr, "x", "", "(shortcut for reverse lookups)")

	// -p option sets the server port
	flag.IntVar(&GlobalQueryOpt_ServerPort, "p", -1, "(specify port number)")

	// -u option displays times in usec instead of msec
	flag.BoolVar(&GlobalQueryOpt_usec, "u", false, "(display times in usec instead of msec)")

	// -v option shows version and exits
	flag.BoolVar(&GlobalQueryOpt_version, "v", false, "(print version and exit)")

	// -m option enables memory tracing
	flag.BoolVar(&GlobalQueryOpt_memtrace, "m", false, "(enable memory usage debugging)")

	// -r disables reading from ~/.digrc
	flag.BoolVar(&GlobalQueryOpt_disabledigrc, "r", false, "(do not read ~/.digrc)")

	// check if sys argv contains "-r", this must be done before flags parse
	for _, arg := range os.Args[1:] {
		if arg == "-r" {
			GlobalQueryOpt_disabledigrc = true
		}
	}

	var allArgs []string
	if !GlobalQueryOpt_disabledigrc {
		digrcArgs, _ := readDigrc(defautDigRCPath())
		// print the args from digrc
		allArgs = append(digrcArgs, os.Args[1:]...)
	} else {
		allArgs = os.Args[1:]
	}
	flag.CommandLine.Parse(allArgs)

	// handle the specific q-opt overrides
	if GlobalQueryOpt_qclass_str != "" {
		qclass, ok := dns.StringToClass[strings.ToUpper(GlobalQueryOpt_qclass_str)]
		if ok {
			GlobalQueryOpt_qclass = qclass
		} else {
			panic("unknown query class: " + GlobalQueryOpt_qclass_str)
		}
	}

	if GLobalQueryOpt_qtype_str != "" {
		upperType := strings.ToUpper(GLobalQueryOpt_qtype_str)
		if strings.HasPrefix(upperType, "IXFR=") {
			// get the serial number
			serial, err := strconv.Atoi(upperType[5:])
			if err != nil {
				panic("invalid IXFR serial number: " + upperType[5:])
			}
			GlobalQueryOpt_ixfr_serial = uint32(serial)
			GlobalQueryOpt_qtype = dns.TypeIXFR
			upperType = "IXFR"

		}

		qtype, ok := dns.StringToType[upperType]
		if ok {
			GlobalQueryOpt_qtype = qtype
			GlobalQueryOpt_HAVETYPE = true
		}
	}

	if GlobalQueryOpt_reverseAddr != "" {
		// check if the reverse address is valid IPv4 or IPv6
		ip := net.ParseIP(GlobalQueryOpt_reverseAddr)
		if ip == nil {
			panic("invalid reverse address: " + GlobalQueryOpt_reverseAddr)
		}

		// set the query type to PTR
		GlobalQueryOpt_qtype = dns.TypePTR
		GlobalQueryOpt_HAVETYPE = true

		var ptrName string
		// If its IPv4, reverse the address and add .in-addr.arpa
		if ip.To4() != nil {
			ptrName = reverseIP(ip) + ".in-addr.arpa"
		} else if ip.To16() != nil {
			// If its IPv6, reverse the address and add .ip6.arpa
			ptrName = reverseIP(ip) + ".ip6.arpa"
		} else {
			panic("invalid address passed to -x: " + GlobalQueryOpt_reverseAddr)
		}

		// set the query name to the reverse address
		GlobalQueryOpt_name = ptrName
		GlobalQueryOpt_HAVENAME = true

	}

	if GlobalQueryOpt_ServerPort != -1 {
		GlobalQueryOpt_HAVEPORT = true
	} else {
		GlobalQueryOpt_ServerPort = 53
	}

	// handle the specific name override
	if GlobalQueryOpt_name_override != "" {
		GlobalQueryOpt_name = GlobalQueryOpt_name_override
		GlobalQueryOpt_HAVENAME = true
	}

	// -4 and -6 are mutually exclusive
	if GlobalQueryOpt_ipv4 && GlobalQueryOpt_ipv6 {
		panic("cannot use both -4 and -6")
	}

	unnamedArgs := flag.Args()
	queries := parseUnnamedArgs(unnamedArgs)
	return queries
}

func reverseIP(ip net.IP) string {
	// Try IPv4 first
	ipv4 := ip.To4()
	if ipv4 != nil {
		return fmt.Sprintf("%d.%d.%d.%d", ipv4[3], ipv4[2], ipv4[1], ipv4[0])
	}

	// Try IPv6
	ipv6 := ip.To16()
	if ipv6 != nil {
		parts := make([]string, 32)
		for i := 0; i < 16; i++ {
			parts[2*i], parts[2*i+1] = fmt.Sprintf("%x", ipv6[i]>>4), fmt.Sprintf("%x", ipv6[i]&0x0f)
		}

		// Reverse parts
		for i, j := 0, len(parts)-1; i < j; i, j = i+1, j-1 {
			parts[i], parts[j] = parts[j], parts[i]
		}

		return strings.Join(parts, ".")
	}

	return ip.String()
}

func parseUnnamedArgs(unnamedArgs []string) []QueryServerOptCombo {

	// global d options
	// for now we only support one query, but eventually we might add the local d options stuff as well
	MainQueryServerOptCombo := NewQueryServerOptCombo()
	dOpt := &MainQueryServerOptCombo.DOptions

	// loop through all the args. if one starts with an @ it is the server
	// to connect to. if one starts with a + it is a d option.

	for _, arg := range unnamedArgs {

		if arg[0] == '@' {
			MainQueryServerOptCombo.Server = arg[1:]
			continue
		}
		if arg[0] == '+' {
			parseDOption(dOpt, arg[1:])
			continue
		}

		upperArg := strings.ToUpper(arg)

		// if the arg is a known query type (a, aaaa, mx, etc) then set the query type
		qtype, ok := dns.StringToType[upperArg]
		if ok {
			GlobalQueryOpt_qtype = qtype
			GlobalQueryOpt_HAVETYPE = true
			continue
		}

		qclass, ok := dns.StringToClass[upperArg]
		if ok {
			GlobalQueryOpt_qclass = qclass
			continue
		}

		// if we're here, it's probably the name
		GlobalQueryOpt_name = arg
		GlobalQueryOpt_HAVENAME = true
	}

	// return the main query server opt combo for now
	return []QueryServerOptCombo{MainQueryServerOptCombo}
}

func parseDOption(dOpt *DOptions, queryOption string) {
	// lowercase queryoptions
	queryOption = strings.ToLower(queryOption)

	// split query option into key and value if there is a = sign
	// if there is no = sign, set the value to true

	var key string
	var value string

	var positive = true
	// if the queryOption starts with "no"	then set positive to false
	if strings.HasPrefix(queryOption, "no") {
		positive = false
		queryOption = queryOption[2:]
	}

	if strings.Contains(queryOption, "=") {
		kv := strings.SplitN(queryOption, "=", 2)
		key = kv[0]
		value = kv[1]
	} else {
		key = queryOption
		value = "true"
	}

	// print the key and value
	//fmt.Println("key: " + key + " value: " + value + " positive: " + strconv.FormatBool(positive))

	// parse the query option

	switch key {
	case "dnssec":
		dOpt.do = positive

	case "bufsize":
		ednsbufsize, err := strconv.Atoi(value)
		if err != nil {
			ednsbufsize = int(DEFAULT_EDNS_BUFSIZE)
		}
		dOpt.ednsbufsize = uint16(ednsbufsize)

	case "padding":
		MAX_PADDING := 512
		padding, err := strconv.Atoi(value)
		if err != nil {
			padding = 0
		}
		if padding > MAX_PADDING {
			padding = MAX_PADDING
		}
		dOpt.ednsPadding = uint8(padding)

	case "timeout":
		timeout, err := strconv.Atoi(value)
		if err != nil {
			timeout = 5
		}
		dOpt.timeout = uint16(timeout)

	case "rdflag", "recurse", "rec":
		dOpt.rd = positive

	case "qr":
		dOpt.qr = positive

	case "cd", "cdflag":
		dOpt.cd = positive

	case "aa", "aaflag", "aaonly":
		dOpt.aa = positive

	case "ad", "adflag":
		dOpt.ad = positive

	case "ra", "raflag":
		dOpt.ra = positive

	case "tc", "tcflag":
		dOpt.tc = positive

	case "tls":
		dOpt.tls_enable = positive

	case "tls-hostname":
		if positive {
			dOpt.tls_hostname = value
		} else {
			dOpt.tls_hostname = ""
		}

	case "tls-ca":
		if positive {
			if value == "true" {
				dOpt.tls_ca = ""
			} else {
				dOpt.tls_ca = value
			}
			dOpt.tls_validate = true
		} else {
			dOpt.tls_ca = ""
			dOpt.tls_validate = false
		}

	case "tls-certfile":
		if positive {
			dOpt.tls_client_certfile = value
		} else {
			dOpt.tls_client_certfile = ""
		}

	case "tls-keyfile":
		if positive {
			dOpt.tls_client_keyfile = value
		} else {
			dOpt.tls_client_keyfile = ""
		}

	case "https":
		dOpt.doh_enable = positive
		dOpt.doh_use_https = positive
		dOpt.doh_use_get = !positive
		if value != "true" {
			dOpt.doh_path = value
		}
	case "https-get":
		dOpt.doh_enable = positive
		dOpt.doh_use_get = positive
		dOpt.doh_use_https = positive
		if value != "true" {
			dOpt.doh_path = value
		}
	case "http-plain":
		dOpt.doh_enable = positive
		dOpt.doh_use_https = !positive
		dOpt.doh_use_get = !positive
		if value != "true" {
			dOpt.doh_path = value
		}
	case "http-plain-get":
		dOpt.doh_enable = positive
		dOpt.doh_use_https = false
		dOpt.doh_use_get = positive
		if value != "true" {
			dOpt.doh_path = value
		}

	case "nsid":
		dOpt.nsid = positive

	case "cookie":
		if positive {
			if value == "true" {
				// generate a random cookie
				cookie, err := GenerateEDNSCookie()
				if err != nil {
					panic(err)
				}
				dOpt.ednsCookie = cookie
			} else {
				dOpt.ednsCookie = value
			}
		} else {
			dOpt.ednsCookie = ""
		}

	case "all":
		UpdateAllDisplay(dOpt, positive)
	case "answer", "ans":
		dOpt.display_answer = positive

	case "authority", "auth":
		dOpt.display_authority = positive

	case "question":
		dOpt.display_question = positive

	case "rrcomments":
		dOpt.display_rrcomments = positive

	case "comments":
		dOpt.display_comments = positive

	case "stats":
		dOpt.display_stats = positive

	case "cmd":
		dOpt.display_cmd = positive

	case "additional":
		dOpt.display_additional = positive

	case "short":
		UpdateAllDisplay(dOpt, false)
		dOpt.display_answer = positive
		dOpt.display_short = positive

	case "tcp", "vc":
		dOpt.tcp = positive

	case "qid":
		// value must be a number
		qid, err := strconv.Atoi(value)
		if err != nil {
			panic(err)
		}
		dOpt.qid = uint16(qid)

	case "onesoa":
		dOpt.onesoa = positive

	case "idnin":
		dOpt.idnin = positive

	case "idnout":
		dOpt.idnout = positive

	case "header-only":
		dOpt.header_only = positive

	case "opcode":
		if positive {
			opcode, err := strconv.Atoi(value)
			if err != nil {
				upperopcode := strings.ToUpper(value)
				dOpt.opcode = dns.StringToOpcode[upperopcode]
			} else {
				dOpt.opcode = opcode
			}
		} else {
			dOpt.opcode = dns.OpcodeQuery
		}

	case "tries":
		tries, err := strconv.Atoi(value)
		if err != nil {
			tries = 3
		}
		if tries < 1 {
			tries = 1
		}
		dOpt.tries = tries

	case "retry":
		retries, err := strconv.Atoi(value)
		if err != nil {
			retries = 2
		}
		if retries < 0 {
			retries = 0
		}
		dOpt.tries = retries + 1

	case "ttlid":
		dOpt.display_ttlid = positive

	case "ttlunits":
		dOpt.display_ttlunits = positive
		if positive {
			dOpt.display_ttlid = true
		}

	case "class":
		dOpt.display_class = positive

	case "crypto":
		dOpt.display_crypto = positive

	case "split":
		if positive {
			split, err := strconv.Atoi(value)
			if err != nil {
				split = int(DEFAULT_SPLIT_CHARS)
			}
			dOpt.split = uint16(split)
		} else {
			dOpt.split = 0
		}

	case "multi", "multiline":
		dOpt.display_multi_line = positive
		// with +multiline, also enable rrcomments
		if positive {
			dOpt.display_rrcomments = true
		}

	case "expandaaaa":
		dOpt.expand_aaaa = positive

	case "edns":
		if positive {
			version, err := strconv.Atoi(value)
			if err != nil {
				version = 0
			}
			dOpt.ednsVersion = uint8(version)
		} else {
			dOpt.ednsVersion = 0
		}

	case "subnet":
		if positive {
			dOpt.ednsClientSubnet = value
		} else {
			dOpt.ednsClientSubnet = ""
		}

	case "zflag": // z flag in the dns header
		dOpt.zflag = positive

	case "expire": // edns expire
		dOpt.ednsExpire = positive

	case "ednsflags": // z bits in the edns header
		if positive {
			// parse octal, hex or decimal
			base := 10 // default to decimal

			// Detect base from prefix
			if len(value) > 2 {
				if value[:2] == "0x" {
					base = 16 // hexadecimal
					value = value[2:]
				} else if value[0] == '0' {
					base = 8 // octal
					value = value[1:]
				}
			}
			num, err := strconv.ParseUint(value, base, 32)
			if err != nil {
				// show error message and exit
				fmt.Println("Invalid ednsflags value: " + value)
				os.Exit(1)
			}
			dOpt.ednsFlags = uint16(num)
		} else {
			dOpt.ednsFlags = 0
		}

	case "ednsnegotiation", "ednsneg":
		dOpt.ednsNegotiation = positive

	case "ednsopt":
		if positive {
			// value is a string of the form "code:value" or just "code"
			// where code is a number and value is a string
			// multiple ednsopt options can be passed

			// split the value by commas
			ednsopts := strings.Split(value, ",")
			for _, ednsopt := range ednsopts {
				// split the ednsopt by colon
				ednsoptKV := strings.SplitN(ednsopt, ":", 2)
				// parse the code
				code, err := strconv.Atoi(ednsoptKV[0])
				if err != nil {
					// show error message and exit
					fmt.Println("Invalid ednsopt code: " + ednsoptKV[0])
					os.Exit(1)
				}
				// get the value
				var ednsval string
				if len(ednsoptKV) == 2 {
					ednsval = ednsoptKV[1]
				} else {
					ednsval = ""
				}
				// create the ednsopt
				ednsopt, err := makeEDNSOption(uint16(code), ednsval)
				if err != nil {
					// show error message and exit
					fmt.Println("Invalid ednsopt value: " + ednsoptKV[1])
					os.Exit(1)
				}
				// append the ednsopt to the list
				dOpt.ednsopt = append(dOpt.ednsopt, ednsopt)
			}
		} else {
			dOpt.ednsopt = nil
		}
	case "showbadcookie":
		dOpt.display_badcookie = positive

	case "nssearch":
		dOpt.nssearch = positive

	default:
		// show error message and exit
		fmt.Println("Unknown query option: " + key)
		os.Exit(1)

	}

}

func UpdateAllDisplay(dOpt *DOptions, positive bool) {
	dOpt.display_answer = positive
	dOpt.display_authority = positive
	dOpt.display_question = positive
	dOpt.display_answer = positive
	dOpt.display_comments = positive
	dOpt.display_stats = positive
	dOpt.display_cmd = positive
	dOpt.display_additional = positive
}

func GenerateEDNSCookie() (string, error) {
	b := make([]byte, 16)
	_, err := rand.Read(b)
	if err != nil {
		return "", err
	}
	return hex.EncodeToString(b), nil
}

func defautDigRCPath() string {
	usr, _ := user.Current()
	digrcPath := filepath.Join(usr.HomeDir, ".digrc")
	return digrcPath
}

func readDigrc(digrcPath string) ([]string, error) {

	file, err := os.Open(digrcPath)
	if err != nil {
		return nil, err
	}
	defer file.Close()

	var args []string
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		line := strings.TrimSpace(scanner.Text())
		if line != "" && !strings.HasPrefix(line, "#") { // Ignore empty lines and comments
			lineArgs := strings.Fields(line) // Split line by spaces
			args = append(args, lineArgs...)
		}
	}

	if err := scanner.Err(); err != nil {
		return nil, err
	}

	return args, nil
}
