package main

var (
	WIG_VERSION           string = "0.0.6-git"
	DEFAULT_EDNS_BUFSIZE  uint16 = 1232
	DEFAULT_TIMEOUT       uint16 = 5
	MULTILINE_SPLIT_CHARS uint16 = 44
	DEFAULT_SPLIT_CHARS   uint16 = 56
)
