package main

import (
	"fmt"
	"os"
	"runtime/trace"
	"strconv"
	"strings"

	"github.com/miekg/dns"
	"golang.org/x/net/idna"
)

func main() {
	cmdqueryopts := parseArgs()

	// if -v is set, print version and exit
	if GlobalQueryOpt_version {
		fmt.Println("Wig " + WIG_VERSION)
		return
	}

	// if -m is set, enable tracing on stderr
	if GlobalQueryOpt_memtrace {
		fmt.Println(";; writing trace data to wig-trace.out")
		fmt.Println(";; use 'go tool trace wig-trace.out' to view trace data")
		f, err := os.Create("wig-trace.out")
		if err != nil {
			panic(err)
		}
		defer f.Close()

		err = trace.Start(f)
		if err != nil {
			panic(err)
		}
		defer trace.Stop()
	}

	for _, cmdqueryopt := range cmdqueryopts {
		queryoptions := BuildQueryOptions(cmdqueryopt)

		// axfr ?
		if GlobalQueryOpt_qtype == dns.TypeAXFR || GlobalQueryOpt_qtype == dns.TypeIXFR {
			transferdata, err := doXFR(queryoptions)
			if err != nil {
				panic(err)
			}

			soa_seen := false

			for ex := range transferdata {
				if ex.Error != nil {
					panic(ex.Error)
				}
				for _, rr := range ex.RR {
					// if queryoptions.onesoa is set, only print the first soa record
					if queryoptions.onesoa && soa_seen && rr.Header().Rrtype == dns.TypeSOA {
						continue
					} else if rr.Header().Rrtype == dns.TypeSOA {
						soa_seen = true
					}
					fmt.Println(rr)
				}
			}

		} else if queryoptions.nssearch {
			nssearch(queryoptions)
		} else {
			question := buildQuery(queryoptions)

			digOutput_BeforeQuery(queryoptions, question)
			responseContext, err := sendQuery(question, queryoptions)
			if err != nil {
				panic(err)
			}
			digOutput(queryoptions, responseContext)
		}

	}

}

func BuildQueryOptions(cmdqueryopt QueryServerOptCombo) QueryOptions {
	queryoptions := NewQueryOptions()

	if GlobalQueryOpt_ipv4 {
		queryoptions.force_ipv4 = true
	} else if GlobalQueryOpt_ipv6 {
		queryoptions.force_ipv6 = true
	}

	// set server
	if cmdqueryopt.Server != "" {
		ips, err := lookupIPs(cmdqueryopt.Server)
		if err != nil {
			panic(err)
		}
		queryoptions.server_ips = ips
		queryoptions.server_hostname = cmdqueryopt.Server
	} else {
		// use first system resolver
		resolvers, err := GetSystemResolvers()
		if err != nil {
			panic(err)
		}
		if len(resolvers) == 0 {
			panic("No system resolvers found")
		}

		resolvers = filterIPs(resolvers)
		queryoptions.server_ips = resolvers

	}

	// set timeout
	queryoptions.timeout = cmdqueryopt.DOptions.timeout

	// set flags
	queryoptions.do = cmdqueryopt.DOptions.do
	queryoptions.rd = cmdqueryopt.DOptions.rd
	queryoptions.cd = cmdqueryopt.DOptions.cd
	queryoptions.aa = cmdqueryopt.DOptions.aa
	queryoptions.ad = cmdqueryopt.DOptions.ad
	queryoptions.ra = cmdqueryopt.DOptions.ra
	queryoptions.tc = cmdqueryopt.DOptions.tc

	// z-flag
	queryoptions.zflag = cmdqueryopt.DOptions.zflag

	// question
	queryoptions.qr = cmdqueryopt.DOptions.qr

	// ixfr_serial
	queryoptions.ixfr_serial = GlobalQueryOpt_ixfr_serial

	// tsig
	if GlobalQueryOpt_tsig != "" {
		tsig_fields := strings.Split(GlobalQueryOpt_tsig, ":")
		if len(tsig_fields) != 3 {
			queryoptions.tsig_algo = "hmac-sha256"
			queryoptions.tsig_keyname = tsig_fields[0]
			queryoptions.tsig_secret = tsig_fields[1]
		} else {
			queryoptions.tsig_algo = tsig_fields[0]
			queryoptions.tsig_keyname = tsig_fields[1]
			queryoptions.tsig_secret = tsig_fields[2]
		}
	} else if GlobalQueryOpt_tsigfile != "" {
		algo, keyname, secret, err := ReadTSIGFile(GlobalQueryOpt_tsigfile)
		if err != nil {
			panic(err)
		}
		queryoptions.tsig_algo = algo
		queryoptions.tsig_keyname = keyname
		queryoptions.tsig_secret = secret
	}

	// edns
	queryoptions.ednsBufSize = cmdqueryopt.DOptions.ednsbufsize
	queryoptions.ednsPadding = cmdqueryopt.DOptions.ednsPadding
	queryoptions.nsid = cmdqueryopt.DOptions.nsid
	queryoptions.ednsCookie = cmdqueryopt.DOptions.ednsCookie
	queryoptions.ednsVersion = cmdqueryopt.DOptions.ednsVersion
	queryoptions.ednsClientSubnet = cmdqueryopt.DOptions.ednsClientSubnet
	queryoptions.ednsFlags = cmdqueryopt.DOptions.ednsFlags
	queryoptions.ednsExpire = cmdqueryopt.DOptions.ednsExpire
	queryoptions.ednsopt = cmdqueryopt.DOptions.ednsopt

	// port
	queryoptions.serverport = uint16(GlobalQueryOpt_ServerPort)

	// question id
	queryoptions.qid = cmdqueryopt.DOptions.qid

	// onesoa
	queryoptions.onesoa = cmdqueryopt.DOptions.onesoa

	// header_only
	queryoptions.header_only = cmdqueryopt.DOptions.header_only

	// idnout
	queryoptions.idnout = cmdqueryopt.DOptions.idnout

	// tcp
	if cmdqueryopt.DOptions.tcp {
		queryoptions.tcp = true
	}
	// tls
	if cmdqueryopt.DOptions.tls_enable {
		if !GlobalQueryOpt_HAVEPORT {
			queryoptions.serverport = 853
		}
		queryoptions.tls_enable = true
		queryoptions.tls_validate = cmdqueryopt.DOptions.tls_validate
		queryoptions.tls_ca = cmdqueryopt.DOptions.tls_ca
		queryoptions.tls_hostname = cmdqueryopt.DOptions.tls_hostname
		queryoptions.tls_client_certfile = cmdqueryopt.DOptions.tls_client_certfile
		queryoptions.tls_client_keyfile = cmdqueryopt.DOptions.tls_client_keyfile

	}

	// doh
	if cmdqueryopt.DOptions.doh_enable {
		if !GlobalQueryOpt_HAVEPORT {
			if cmdqueryopt.DOptions.doh_use_https {
				queryoptions.serverport = 443
			} else {
				queryoptions.serverport = 80
			}
		}
		queryoptions.doh_enable = true
		queryoptions.doh_use_https = cmdqueryopt.DOptions.doh_use_https
		queryoptions.doh_path = cmdqueryopt.DOptions.doh_path
		queryoptions.doh_use_get = cmdqueryopt.DOptions.doh_use_get
	}

	// if we have neither name or type, set type = NS and name = .
	if !GlobalQueryOpt_HAVENAME && !GlobalQueryOpt_HAVETYPE {
		queryoptions.qtype = dns.TypeNS
		queryoptions.name = "."
	}

	// display options
	queryoptions.display_answer = cmdqueryopt.DOptions.display_answer
	queryoptions.display_authority = cmdqueryopt.DOptions.display_authority
	queryoptions.display_additional = cmdqueryopt.DOptions.display_additional
	queryoptions.display_class = cmdqueryopt.DOptions.display_class
	queryoptions.display_cmd = cmdqueryopt.DOptions.display_cmd
	queryoptions.display_comments = cmdqueryopt.DOptions.display_comments
	queryoptions.display_crypto = cmdqueryopt.DOptions.display_crypto
	queryoptions.display_question = cmdqueryopt.DOptions.display_question
	queryoptions.display_rrcomments = cmdqueryopt.DOptions.display_rrcomments
	queryoptions.display_short = cmdqueryopt.DOptions.display_short
	queryoptions.display_stats = cmdqueryopt.DOptions.display_stats
	queryoptions.display_ttlid = cmdqueryopt.DOptions.display_ttlid
	queryoptions.display_ttlunits = cmdqueryopt.DOptions.display_ttlunits

	queryoptions.display_badcookie = cmdqueryopt.DOptions.display_badcookie
	queryoptions.display_search = cmdqueryopt.DOptions.display_search
	if cmdqueryopt.DOptions.split > 0 {
		// find the closest multiple of 4
		queryoptions.split = ((cmdqueryopt.DOptions.split + 3) / 4) * 4
	}
	queryoptions.display_multi_line = cmdqueryopt.DOptions.display_multi_line
	if queryoptions.display_multi_line && queryoptions.split == 0 {
		queryoptions.split = MULTILINE_SPLIT_CHARS
	}

	queryoptions.expand_aaaa = cmdqueryopt.DOptions.expand_aaaa
	queryoptions.usec = GlobalQueryOpt_usec

	// idnin
	if cmdqueryopt.DOptions.idnin {
		ascii_name, err := idna.ToASCII(queryoptions.name)
		if err != nil {
			panic(err)
		}
		queryoptions.name = ascii_name
	}

	// opcode
	queryoptions.opcode = cmdqueryopt.DOptions.opcode

	// local bind address / port
	localbindstring := GlobalQueryOpt_localbind
	if localbindstring != "" {
		localbindfields := strings.Split(localbindstring, "#")
		if len(localbindfields) == 2 {
			queryoptions.localbind = localbindfields[0]
			port, err := strconv.Atoi(localbindfields[1])
			if err != nil {
				panic(err)
			}
			queryoptions.localbindport = uint16(port)
		} else {
			queryoptions.localbind = localbindstring
		}
	}

	queryoptions.tries = cmdqueryopt.DOptions.tries

	// nssearch
	queryoptions.nssearch = cmdqueryopt.DOptions.nssearch

	return queryoptions
}
