// write a test for reverseIP() in commandline_test.go
package main

import (
	"net"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestReverseIP(t *testing.T) {
	ip := net.ParseIP("2001:620:0:ff::5c")
	if ip == nil {
		t.Fatal("invalid IP")
	}
	reverse := reverseIP(ip)
	expected := "c.5.0.0.0.0.0.0.0.0.0.0.0.0.0.0.f.f.0.0.0.0.0.0.0.2.6.0.1.0.0.2"

	assert.Equal(t, expected, reverse)

}
