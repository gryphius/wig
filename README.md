The aim of `wig` is to provide a simple command that can be used like `dig` but is also easily installable on windows.

The tool is basically a wrapper around the excellent [miekg/dns](https://github.com/miekg/dns) library by Miek Gieben which does all the heavy lifting.

# but why?

I was frustrated that there is apparently no easy way to AXFR a zone with TSIG from windows ( nslookup and powershell commands apparently do not support TSIG ). 
As I'm used to using `dig`  a lot on other OS's I thought it would be nice to have a tool that can be used the same way. Current versions of `dig` are not available for windows anymore. 

Apart from that, wig is mostly a learning experience for me, getting to know all of dig's advanced options and the more obscure parts of the DNS protocol.

# Status

`wig` is "beta" quality, has likely a few bugs and is missing some features of `dig`, but still usable for most cases.

See [dig-compatibility.md](dig-compatibility.md) for a list of `dig` options and their implementation status in `wig`.


# Notes

 * Windows Defender may detect the binary as `Trojan:Win32/Bearfoos.A!ml` *shrug*
 * On windows cmd, some arguments may need quotes, for example `wig.exe "@8.8.8.8" slashdot.org`
 * command flags (`-t type` , `-x`, etc) must be passed before the  options (`+ans`, `@8.8.8.8` etc..)

# Examples

look up the IPv6 address for `jpmens.org` using the system's default resolver:

```
>wig.exe -t AAAA jpmens.org

<<>>🦱 Wig 0.0.4 <<>>  -t AAAA jpmens.org
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- ;; opcode: QUERY, status: NOERROR, id: 16951
;; flags: qr rd ra ad; QUERY: 1 ANSWER: 1 AUTHORITY: 0 ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version 0; flags:; udp: 1232
;; QUESTION SECTION:
;;jpmens.org.   IN       AAAA

;; ANSWER SECTION:
jpmens.org.     7200    IN      AAAA    2a00:d0c0:200:0:b9:1a:9c:48

;; Query time: 53 msec
;; SERVER: 2620:fe::fe#53() (UDP)
;; WHEN: Wed Sep 13 06:08:22 CEST 2023
;; MSG SIZE  rcvd: 77
```

get the location from your current google resolver:
("gpdns-zrh" -> Zürich )

```
wig.exe +noall +comments +nsid "@8.8.8.8"

;; Got answer:
;; ->>HEADER<<- ;; opcode: QUERY, status: NOERROR, id: 37268
;; flags: qr rd ra ad; QUERY: 1 ANSWER: 13 AUTHORITY: 0 ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version 0; flags:; udp: 512
; NSID: 67 70 64 6e 73 2d 7a 72 68 ("gpdns-zrh")
```

query using DoH (dns-over-https):
```
wig.exe +noall +ans +https "@dns.switch.ch" switch.ch

switch.ch.      108     IN      A       130.59.31.80
```

query using DoT (dns-over-TLS):

```
wig.exe +tls +noall +ans +stats "@9.9.9.9" quad9.net mx

quad9.net.      300     IN      MX      5 mx1.quad9.net.
quad9.net.      300     IN      MX      20 mx2.quad9.net.
quad9.net.      300     IN      MX      10 mx4.quad9.net.

;; Query time: 3 msec
;; SERVER: 9.9.9.9#853(9.9.9.9) (TLS)
;; WHEN: Wed Sep 13 06:10:10 CEST 2023
;; MSG SIZE  rcvd: 152
```


download the LI zone from SWITCH's zonedata server:

```
wig.exe -t axfr -y hmac-sha512:tsig-zonedata-li-public-21-01:t8GgeCn+fhPaj+cRy1epox2Vj4hZ45ax6v3rQCkkfIQNg5fsxuU23QM5mzz+BxJ4kgF/jiQyBDBvL+XWPE6oCQ== @zonedata.switch.ch li.
li.     900     IN      SOA     a.nic.li. dns-operation.switch.ch. 2023072909 900 600 1123200 900
li.     0       IN      RRSIG   NSEC3PARAM 13 1 0 20230823215954 20230724210007 62951 li. Z9QI4jsBZqtuPROhq1uMeIwBC6HtrFxmqDOiC8IWSoPrYWlOnqXxkttr0G6JiU+ASUcfsmE12ocv2gXPW6bD5A==
li.     0       IN      NSEC3PARAM      1 0 0 -
li.     86400   IN      RRSIG   DNSKEY 13 1 86400 20230912100909 20230728090909 43984 li. dcsm/UXqNOwMR1pc1MHsBhiMsrTt8c90yNz3JEqqZUvJn+dwpBrerV2YAA4DB8GG/FrGTaGt1n0AN0Ex1Ksvhg==
li.     86400   IN      DNSKEY  257 3 13 4XBjccbv0NQOFqjpNtON7DGswnp92x5DdHp6OKDSQXsjEfA7S7x0YUYFeVZ4Db7LyO2bQBbtjPjHd7oiZUw1ew==
li.     86400   IN      DNSKEY  256 3 13 PTuRBISkcVbtdqLJUNVHdFkh3bsH5tPgwdE98dIBjM0qsjxDSy4iArVLEf/pR36BU89H6fCTAovzxYix/86Fhw==
li.     86400   IN      DNSKEY  256 3 13 IIUldP8k1wRpAzbZe3VJ1vGecXDqj4qivR4+Gwq+D8LlUye9EqisMPpazshvWgLY9bfGqKuZPtO4v2wRiKLokQ==
li.     172800  IN      RRSIG   NS 13 1 172800 20230822155632 20230724033500 62951 li. lcBOcLt6e0EOEDTjbxgYS5Q3yShxiRxJAqFTtEUR9ajfJWqcC6Yfvo8dl2v6u+/PV+tAq9SyPHJsd2S3uxvvcw==
li.     172800  IN      NS      a.nic.li.
li.     172800  IN      NS      b.nic.li.
li.     172800  IN      NS      e.nic.li.
li.     172800  IN      NS      f.nic.li.
li.     172800  IN      NS      d.nic.li.
li.     900     IN      RRSIG   SOA 13 1 900 20230828065637 20230729060008 62951 li. FZhGft2OGqmy7Jp202GRjYLr950lBZRzG9wFqvu9ptQZQF/OxozU30oqevcfFOWzw0DBhVCRTfAFzYxYVHl9Hw==
0-0.li. 3600    IN      NS      tessa.ns.cloudflare.com.
0-0.li. 3600    IN      NS      morgan.ns.cloudflare.com.
0-1.li. 3600    IN      RRSIG   DS 13 2 3600 20230828050831 20230729050008 62951 li. 1gYWbRO0XaYN+jhtepqAl9aGWbyZ16tIrKblDjGBwKfH0xJRFORrQX+/mZFp6pjcaf86JSHOQsumI2bQLCrIEA==
[...]
VVVUES8SLOPDJR1EERHV2RV9RRO3KK0F.li.    900     IN      NSEC3   1 1 0 - 0084G91JOT96IJVHFP90EK6I1D9RLKF3 NS DS RRSIG
VVVUES8SLOPDJR1EERHV2RV9RRO3KK0F.li.    900     IN      RRSIG   NSEC3 13 2 900 20230825215102 20230726210008 62951 li. NQbmiWnOvsv4ez/LM9N2LYZfy5pv5lHFIYh3Bb3Jf85Y1wraXtsaXWa0kZ8klQsx/xVDTAQ9gQeURFsK4+Ohbg==
li.     900     IN      SOA     a.nic.li. dns-operation.switch.ch. 2023072909 900 600 1123200 900
```

# Building

`go build` should be enough to build the binary.

