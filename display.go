package main

import (
	"fmt"
	"net"
	"os"
	"strconv"
	"strings"
	"time"

	"github.com/miekg/dns"
	"golang.org/x/net/idna"
)

func digOutput_BeforeQuery(queryoptions QueryOptions, question *dns.Msg) {
	// version and full command line arguments
	if queryoptions.display_cmd {
		fmt.Println("<<>>🦱 Wig "+WIG_VERSION+" <<>> ", strings.Join(os.Args[1:], " "))

		var global_opts_list = []string{"+cmd"}
		global_opts_display := strings.Join(global_opts_list, " ")
		fmt.Println(";; global options: " + global_opts_display) // TODO: what other global options are there? +cmd is one of them
	}

	// print question
	if queryoptions.qr {
		fmt.Println(";; Sending:")
		fmt.Println(question)
	}

}

func displayOptSection(rr *dns.OPT) string {
	s := "\n;; OPT PSEUDOSECTION:\n; EDNS: version " + strconv.Itoa(int(rr.Version())) + "; "
	if rr.Do() {
		s += "flags: do; "
	} else {
		s += "flags:; "
	}
	if rr.Hdr.Ttl&0x7FFF != 0 {
		s += fmt.Sprintf("MBZ: 0x%04x, ", rr.Hdr.Ttl&0x7FFF)
	}
	s += "udp: " + strconv.Itoa(int(rr.UDPSize()))

	for _, o := range rr.Option {
		switch o.(type) {
		case *dns.EDNS0_NSID:
			nsidHexValue := o.String()
			// split in into chunks of 2
			nsidHexValueSplit := split(nsidHexValue, 2, " ")
			s += "\n; NSID: " + nsidHexValueSplit
			// for each of these chunks, convert to ascii
			nsidAsciiValue := ""
			for _, hexValue := range strings.Split(nsidHexValueSplit, " ") {
				asciiValue, err := strconv.ParseInt(hexValue, 16, 32)
				if err != nil {
					nsidAsciiValue += "."
				} else {
					nsidAsciiValue += string(asciiValue)
				}
			}
			s += fmt.Sprintf(" (\"%s\")", nsidAsciiValue)

		case *dns.EDNS0_SUBNET:
			s += "\n; SUBNET: " + o.String()
		case *dns.EDNS0_COOKIE:
			s += "\n; COOKIE: " + o.String()
		case *dns.EDNS0_EXPIRE:
			s += fmt.Sprintf("\n; EXPIRE: %s (%s)", o.String(), secondsToHuman(o.(*dns.EDNS0_EXPIRE).Expire, false))
		case *dns.EDNS0_TCP_KEEPALIVE:
			s += "\n; KEEPALIVE: " + o.String()
		case *dns.EDNS0_UL:
			s += "\n; UPDATE LEASE: " + o.String()
		case *dns.EDNS0_LLQ:
			s += "\n; LONG LIVED QUERIES: " + o.String()
		case *dns.EDNS0_DAU:
			s += "\n; DNSSEC ALGORITHM UNDERSTOOD: " + o.String()
		case *dns.EDNS0_DHU:
			s += "\n; DS HASH UNDERSTOOD: " + o.String()
		case *dns.EDNS0_N3U:
			s += "\n; NSEC3 HASH UNDERSTOOD: " + o.String()
		case *dns.EDNS0_LOCAL:
			s += "\n; LOCAL OPT: " + o.String()
		case *dns.EDNS0_PADDING:
			s += "\n; PADDING: " + o.String()
		case *dns.EDNS0_EDE:
			s += "\n; EDE: " + o.String()
		case *dns.EDNS0_ESU:
			s += "\n; ESU: " + o.String()
		}
	}
	return s
}

func digOutput(queryoptions QueryOptions, responseContext *ResponseContext) {
	response := responseContext.response
	if response == nil {
		return
	}

	opt, otheradditional := splitAdditionalSection(response)

	// print response
	if queryoptions.display_comments {
		fmt.Println(";; Got answer:")
		fmt.Print(";; ->>HEADER<<- ")
		fmt.Print(response.MsgHdr.String())
		// show number of entries in the QUERY, ANSWER, AUTHORITY, ADDITIONAL sections
		fmt.Println(" QUERY:", len(response.Question), "ANSWER:", len(response.Answer), "AUTHORITY:", len(response.Ns), "ADDITIONAL:", len(response.Extra))

		// display OPT PSEUDOSECTION
		for _, opt := range opt {
			fmt.Println(displayOptSection(opt))
		}

	}

	// question
	if queryoptions.display_question {
		if queryoptions.display_comments {
			fmt.Println(";; QUESTION SECTION:")
		}
		for _, q := range response.Question {
			fmt.Println(";" + q.String())
		}
		fmt.Println()
	}

	// answer
	if queryoptions.display_answer && len(response.Answer) > 0 {
		if queryoptions.display_comments {
			fmt.Println(";; ANSWER SECTION:")
		}
		for _, rr := range response.Answer {
			fmt.Println(singleRR(rr, queryoptions))
		}
		fmt.Println()
	}

	// authority
	if queryoptions.display_authority && len(response.Ns) > 0 {
		if queryoptions.display_comments {
			fmt.Println(";; AUTHORITY SECTION:")
		}
		for _, rr := range response.Ns {
			fmt.Println(rr.String())
		}
		fmt.Println()
	}

	// additional
	if queryoptions.display_additional && len(otheradditional) > 0 {
		if queryoptions.display_comments {
			fmt.Println(";; ADDITIONAL SECTION:")
		}
		for _, rr := range otheradditional {
			fmt.Println(rr.String())
		}
		fmt.Println()
	}

	// print stats
	if queryoptions.display_stats {
		// Query time
		if queryoptions.usec {
			fmt.Printf(";; Query time: %d usec\n", responseContext.rtt.Microseconds())
		} else {
			fmt.Printf(";; Query time: %d msec\n", responseContext.rtt.Milliseconds())
		}

		// Server // Protocol
		protocol := "UDP"
		if queryoptions.tcp {
			protocol = "TCP"
		}
		if queryoptions.tls_enable {
			protocol = "TLS"
		}
		if queryoptions.doh_enable {
			protocol = "HTTPS"
			if !queryoptions.doh_use_https {
				protocol = "HTTP"
			}
		}

		fmt.Printf(";; SERVER: %s#%d(%s) (%s)\n", responseContext.serverip, responseContext.serverport, queryoptions.server_hostname, protocol)
		// when
		fmt.Printf(";; WHEN: %s\n", time.Now().Format("Mon Jan 02 15:04:05 MST 2006"))
		// MSG SIZE
		fmt.Printf(";; MSG SIZE  rcvd: %d\n", response.Len())
	}

	//fmt.Println(response)

}

func splitAdditionalSection(m *dns.Msg) ([]*dns.OPT, []dns.RR) {
	var optRecords []*dns.OPT
	var otherRecords []dns.RR

	for _, extra := range m.Extra {
		if opt, ok := extra.(*dns.OPT); ok {
			optRecords = append(optRecords, opt)
		} else {
			otherRecords = append(otherRecords, extra)
		}
	}

	return optRecords, otherRecords
}

// singleRR returns a single RR as a string
func singleRR(rr dns.RR, queryoptions QueryOptions) string {
	var str string

	rr_header := rr.Header()
	str_rdata := getRdata(rr, queryoptions)

	if queryoptions.display_short {
		str = str_rdata
	} else {
		str_rr_name := rr_header.Name
		rr_type := rr_header.Rrtype
		rr_class := rr_header.Class
		rr_ttl := rr_header.Ttl

		sb := strings.Builder{}
		sb.WriteString(str_rr_name)
		if queryoptions.display_ttlid {
			str_ttl := fmt.Sprintf("%d", rr_ttl)
			if queryoptions.display_ttlunits {
				str_ttl = secondsToHuman(rr_ttl, true)
			}
			sb.WriteString(fmt.Sprintf("\t%s", str_ttl))
		}
		if queryoptions.display_class {
			sb.WriteString(fmt.Sprintf("\t%s", dns.ClassToString[rr_class]))
		}

		// add type
		sb.WriteString(fmt.Sprintf("\t%s", dns.TypeToString[rr_type]))
		// add rdata
		sb.WriteString(fmt.Sprintf("\t%s", str_rdata))

		str = sb.String()
	}

	// idn
	if queryoptions.idnout {
		decodedStr, err := decodePunycodeInString(str)
		if err == nil {
			str = decodedStr
		}
	}

	return str
}

// get the rdata by just removing the header from the miekg/dns string representation by default
func getRdata(rr dns.RR, queryoptions QueryOptions) string {

	// for some record types, we need to do something special

	// separator between chunks of data
	sep := " "
	if queryoptions.display_multi_line {
		sep = "\n\t"
	}

	rrcomment := ""
	if queryoptions.display_rrcomments {
		rrcomment = getRRComments(rr)
	}
	switch rr.Header().Rrtype {
	case dns.TypeDS:
		rrDS := rr.(*dns.DS)
		dsDigest := split(rrDS.Digest, int(queryoptions.split), sep)
		if !queryoptions.display_crypto {
			dsDigest = "[omitted]"
		}
		if queryoptions.display_multi_line {
			return fmt.Sprintf("%d %d %d (\n\t%s )%s", rrDS.KeyTag, rrDS.Algorithm, rrDS.DigestType, dsDigest, rrcomment)
		} else {
			return fmt.Sprintf("%d %d %d %s%s", rrDS.KeyTag, rrDS.Algorithm, rrDS.DigestType, dsDigest, rrcomment)
		}

	case dns.TypeCDS:
		rrCDS := rr.(*dns.CDS)
		cdsDigest := split(rrCDS.Digest, int(queryoptions.split), sep)
		if !queryoptions.display_crypto {
			cdsDigest = "[omitted]"
		}
		if queryoptions.display_multi_line {
			return fmt.Sprintf("%d %d %d (\n\t%s )%s", rrCDS.KeyTag, rrCDS.Algorithm, rrCDS.DigestType, cdsDigest, rrcomment)
		} else {
			return fmt.Sprintf("%d %d %d %s%s", rrCDS.KeyTag, rrCDS.Algorithm, rrCDS.DigestType, cdsDigest, rrcomment)
		}

	case dns.TypeDNSKEY:
		rrDNSKEY := rr.(*dns.DNSKEY)
		dnskeyPublicKey := split(rrDNSKEY.PublicKey, int(queryoptions.split), sep)
		if !queryoptions.display_crypto {
			dnskeyPublicKey = fmt.Sprintf("[key id = %d]", rrDNSKEY.KeyTag())
		}
		if queryoptions.display_multi_line {
			return fmt.Sprintf("%d %d %d (\n\t%s )%s", rrDNSKEY.Flags, rrDNSKEY.Protocol, rrDNSKEY.Algorithm, dnskeyPublicKey, rrcomment)
		} else {
			return fmt.Sprintf("%d %d %d %s%s", rrDNSKEY.Flags, rrDNSKEY.Protocol, rrDNSKEY.Algorithm, dnskeyPublicKey, rrcomment)
		}

	case dns.TypeCDNSKEY:
		rrCDNSKEY := rr.(*dns.CDNSKEY)
		cdnskeyPublicKey := split(rrCDNSKEY.PublicKey, int(queryoptions.split), sep)
		if !queryoptions.display_crypto {
			cdnskeyPublicKey = fmt.Sprintf("[key id = %d]", rrCDNSKEY.KeyTag())
		}
		if queryoptions.display_multi_line {
			return fmt.Sprintf("%d %d %d (\n\t%s )%s", rrCDNSKEY.Flags, rrCDNSKEY.Protocol, rrCDNSKEY.Algorithm, cdnskeyPublicKey, rrcomment)
		} else {
			return fmt.Sprintf("%d %d %d %s%s", rrCDNSKEY.Flags, rrCDNSKEY.Protocol, rrCDNSKEY.Algorithm, cdnskeyPublicKey, rrcomment)
		}

	case dns.TypeRRSIG:
		rrRRSIG := rr.(*dns.RRSIG)

		rrsigSignature := split(rrRRSIG.Signature, int(queryoptions.split), sep)
		if !queryoptions.display_crypto {
			rrsigSignature = "[omitted]"
		}
		if queryoptions.display_multi_line {

			return fmt.Sprintf("%d %d %d %d (\n\t%d %d %d\n\t%s )", rrRRSIG.TypeCovered, rrRRSIG.Algorithm, rrRRSIG.Labels, rrRRSIG.OrigTtl, rrRRSIG.Expiration, rrRRSIG.Inception, rrRRSIG.KeyTag, rrsigSignature)
		} else {
			return fmt.Sprintf("%d %d %d %d %d %d %d %s", rrRRSIG.TypeCovered, rrRRSIG.Algorithm, rrRRSIG.Labels, rrRRSIG.OrigTtl, rrRRSIG.Expiration, rrRRSIG.Inception, rrRRSIG.KeyTag, rrsigSignature)
		}
	case dns.TypeAAAA:
		rrAAAA := rr.(*dns.AAAA)
		if queryoptions.expand_aaaa {
			expanded := expandIPv6(rrAAAA.AAAA)
			return expanded
		}
		return rrAAAA.AAAA.String()

	case dns.TypeSOA:
		rrSOA := rr.(*dns.SOA)
		if queryoptions.display_multi_line {
			sb := strings.Builder{}
			sb.WriteString(fmt.Sprintf("%s %s (\n", rrSOA.Ns, rrSOA.Mbox))
			sb.WriteString(fmt.Sprintf("\t%d ; serial\n", rrSOA.Serial))
			sb.WriteString(fmt.Sprintf("\t%d ; refresh (%s)\n", rrSOA.Refresh, secondsToHuman(rrSOA.Refresh, false)))
			sb.WriteString(fmt.Sprintf("\t%d ; retry (%s)\n", rrSOA.Retry, secondsToHuman(rrSOA.Retry, false)))
			sb.WriteString(fmt.Sprintf("\t%d ; expire (%s)\n", rrSOA.Expire, secondsToHuman(rrSOA.Expire, false)))
			sb.WriteString(fmt.Sprintf("\t%d ; minimum (%s)\n", rrSOA.Minttl, secondsToHuman(rrSOA.Minttl, false)))
			sb.WriteString(fmt.Sprintf(")%s", rrcomment))
			return sb.String()
		} else {
			return fmt.Sprintf("%s %s %d %d %d %d %d%s", rrSOA.Ns, rrSOA.Mbox, rrSOA.Serial, rrSOA.Refresh, rrSOA.Retry, rrSOA.Expire, rrSOA.Minttl, rrcomment)
		}
	default:
		rrHeader := rr.Header().String()
		rrWhole := rr.String()

		if strings.HasPrefix(rrWhole, rrHeader) {
			rrRData := strings.TrimPrefix(rrWhole, rrHeader)
			return strings.TrimSpace(rrRData)
		}

		return rrWhole
	}
}

func decodePunycodeInString(input string) (string, error) {
	// Split by spaces
	spaceParts := strings.Split(input, " ")

	for i, spacePart := range spaceParts {
		// Split by dots
		dotParts := strings.Split(spacePart, ".")

		for j, dotPart := range dotParts {
			if strings.HasPrefix(dotPart, "xn--") {
				decodedDotPart, err := idna.ToUnicode(dotPart)
				if err != nil {
					return "", fmt.Errorf("error decoding part '%s': %v", dotPart, err)
				}
				dotParts[j] = decodedDotPart
			}
		}

		// Reassemble the dot-separated parts
		spaceParts[i] = strings.Join(dotParts, ".")
	}

	// Reassemble the space-separated parts
	return strings.Join(spaceParts, " "), nil
}

// convert number of seconds into a human readable string with s,m,h,d,w units
// all units are shown unless they are 0
func secondsToHuman(seconds uint32, shortform bool) string {
	var sb strings.Builder

	// weeks
	weeks := seconds / 604800
	if weeks > 0 {
		timestr := timestring("week", weeks, shortform)
		sb.WriteString(fmt.Sprintf("%d%s", weeks, timestr))
		seconds = seconds % 604800
	}

	// days
	days := seconds / 86400
	if days > 0 {
		if sb.Len() > 0 {
			sb.WriteString(" ")
		}
		timestr := timestring("day", days, shortform)
		sb.WriteString(fmt.Sprintf("%d%s", days, timestr))
		seconds = seconds % 86400
	}

	// hours
	hours := seconds / 3600
	if hours > 0 {
		if sb.Len() > 0 {
			sb.WriteString(" ")
		}
		timeStr := timestring("hour", hours, shortform)
		sb.WriteString(fmt.Sprintf("%d%s", hours, timeStr))
		seconds = seconds % 3600
	}

	// minutes
	minutes := seconds / 60
	if minutes > 0 {
		if sb.Len() > 0 {
			sb.WriteString(" ")
		}
		timeStr := timestring("minute", minutes, shortform)
		sb.WriteString(fmt.Sprintf("%d%s", minutes, timeStr))
		seconds = seconds % 60
	}

	// seconds
	if seconds > 0 {
		if sb.Len() > 0 {
			sb.WriteString(" ")
		}
		timeStr := timestring("second", seconds, shortform)
		sb.WriteString(fmt.Sprintf("%d%s", seconds, timeStr))
	}

	return sb.String()
}

// split spring into parts of length n and join them with a space
func split(s string, chunkSize int, join string) string {
	return strings.Join(splitStringIntoChunks(s, chunkSize), join)
}

func timestring(s string, n uint32, shortform bool) string {
	if shortform {
		return s[0:1]
	}
	if n == 1 {
		return " " + s
	}
	return " " + s + "s"
}

// split spring into parts of length n
func splitStringIntoChunks(s string, chunkSize int) []string {
	if chunkSize <= 0 {
		return []string{s}
	}

	var chunks []string

	for start := 0; start < len(s); start += chunkSize {
		end := start + chunkSize

		// Ensure we don't exceed the length of the string
		if end > len(s) {
			end = len(s)
		}

		chunks = append(chunks, s[start:end])
	}

	return chunks
}

func expandIPv6(ip net.IP) string {
	// Convert to 16-byte representation
	ip = ip.To16()

	// Create groups of 4 hexadecimal digits
	groups := make([]string, 8)
	for i := 0; i < 8; i++ {
		group := (uint16(ip[i*2]) << 8) | uint16(ip[i*2+1])
		groups[i] = fmt.Sprintf("%04x", group)
	}

	return strings.Join(groups, ":")
}

func getRRComments(rr dns.RR) string {
	switch rr.Header().Rrtype {
	case dns.TypeDNSKEY:
		rrDNSKEY := rr.(*dns.DNSKEY)
		keytype := "ZSK"
		if rrDNSKEY.Flags&dns.SEP != 0 {
			keytype = "KSK"
		}
		keyAlg := dns.AlgorithmToString[rrDNSKEY.Algorithm]
		return fmt.Sprintf(" ; %s; alg = %s ; key id = %d", keytype, keyAlg, rrDNSKEY.KeyTag())
	case dns.TypeCDNSKEY:
		rrCDNSKEY := rr.(*dns.CDNSKEY)
		keytype := "ZSK"
		if rrCDNSKEY.Flags&dns.SEP != 0 {
			keytype = "KSK"
		}
		keyAlg := dns.AlgorithmToString[rrCDNSKEY.Algorithm]
		return fmt.Sprintf(" ; %s; alg = %s ; key id = %d", keytype, keyAlg, rrCDNSKEY.KeyTag())
	}
	return ""
}
